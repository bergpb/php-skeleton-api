### Simple PHP API

Simple PHP API to be used as skeleton in another projects.

Running (with docker and docker compose):

1. `docker compose up -d`
1. Run seed accessing this URL: [http://localhost:8000/dbseed](http://localhost:8000/dbseed)
1. Application will be running on: [localhost:8000](localhost:8000)
1. PHPMyAdmin will be running on: [localhost:8001](localhost:8001)

<?php
namespace Src\Controllers;

use Src\Models\Person;

class PersonController extends BaseController {

    private $requestMethod;
    private $userId;

    private $person;

    public function __construct($db, $requestMethod, $userId)
    {
        $this->db = $db;
        $this->userId = $userId;
        $this->requestMethod = $requestMethod;

        $this->person = new Person($db);
    }

    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                if ($this->userId) {
                    $this->res = $this->getUser($this->userId);
                } else {
                    $this->res = $this->getAllUsers();
                };
                break;
            case 'POST':
                $this->res = $this->createUserFromRequest();
                break;
            case 'PUT':
                $this->res = $this->updateUserFromRequest($this->userId);
                break;
            case 'DELETE':
                $this->res = $this->deleteUser($this->userId);
                break;
            default:
                $this->res = $this->notFoundResponse();
                break;
        }
    }

    private function getAllUsers()
    {
        $result = $this->person->findAll();
        $this->statusCode = self::HTTP_OK;
        $this->res['body'] = json_encode($result);
        $this->returnRespose();
    }

    private function getUser($id)
    {
        $result = $this->person->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $this->statusCode = self::HTTP_OK;
        $this->res['body'] = json_encode($result);
        $this->returnRespose();
    }

    private function createUserFromRequest()
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (! $this->validatePerson($input)) {
            return $this->unprocessableEntityResponse();
        }
        $this->person->insert($input);
        $this->statusCode = self::HTTP_CREATED;
        $this->returnRespose();
    }

    private function updateUserFromRequest($id)
    {
        $result = $this->person->find($id);
        if (empty($result)) {
            return $this->notFoundResponse();
        }
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (! $this->validatePerson($input)) {
            return $this->unprocessableEntityResponse();
        }
        $this->person->update($id, $input);
        $this->statusCode = self::HTTP_OK;
        $this->returnRespose();
    }

    private function deleteUser($id)
    {
        $result = $this->person->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $this->person->delete($id);
        $this->statusCode = self::HTTP_OK;
        $this->returnRespose();
    }

    private static function validatePerson($input)
    {
        if (! isset($input['firstname'])) {
            return false;
        }
        if (! isset($input['lastname'])) {
            return false;
        }
        return true;
    }

}

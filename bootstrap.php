<?php
require 'vendor/autoload.php';

use Src\Database\DatabaseConnector;

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$con = new DatabaseConnector();
$dbConnection = $con->getConnection();

<?php
namespace Src\Controllers;

class BaseController
{

    public const HTTP_OK = 200;
    public const HTTP_CREATED = 201;
    public const HTTP_BAD_REQUEST = 400;
    public const HTTP_NOT_FOUND = 404;
    public const HTTP_UNPROCESSABLE_ENTITY = 422;
    public const HTTP_INTERNAL_SERVER_ERROR = 500;

    public const HTTP_VERSION = "HTTP/1.1";

    public static $statusTexts = [
        200 => 'OK',
        201 => 'Created',
        400 => 'Bad Request',
        404 => 'Not Found',
        422 => 'Unprocessable Content',
        500 => 'Internal Server Error',
    ];

    public function returnRespose()
    {
        header(sprintf("%s %s %s", self::HTTP_VERSION, $this->statusCode, self::$statusTexts[$this->statusCode]));
        if ($this->res['body']) {
            echo $this->res['body'];
        }
        exit();
    }

    public function notFoundResponse()
    {
        header(sprintf("%s %s %s", self::HTTP_VERSION, 404, self::$statusTexts[404]));
        echo json_encode(['error' => 'Not Found']);
        exit();
    }

    public function unprocessableEntityResponse()
    {
        header(sprintf("%s %s %s", self::HTTP_VERSION, 422, self::$statusTexts[422]));
        echo json_encode(['error' => 'Invalid Input']);
        exit();
    }

}
